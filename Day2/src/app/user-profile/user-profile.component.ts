import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  constructor() { }
  user: User;

  ngOnInit(): void {
    this.user = {
      id: 1,
      username: "haihoangm",
      fullname: "Hoang Manh Hai",
      phonenumber: "036523823*",
      email: "haihoangm@nashtechglobal.com",
      address: "Cau Giay, Ha Noi",
      password: "",
      isActive: true,
      avartar: "https://static.postize.com/images/ezcrRYELjBgS_984.jpg",
      token: "",
    }
  }

}
