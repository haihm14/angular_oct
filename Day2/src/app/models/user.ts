
export interface User {
    id: number;
    username: string;
    fullname: string;
    phonenumber?: string;
    email?: string;
    address: string;
    password: string;
    isActive: boolean;
    avartar?: string;
    token?: string;
}